from __future__ import print_function
import requests
import json
import cv2
import os 
import psycopg2

addr = 'http://localhost:5000'
test_url = addr + '/gender'

def search_and_send():
	path = "DB"
	content_type = 'image/jpeg'
	headers = {'content-type': content_type}
	gender_dictionary = {}
	for file in os.listdir(path):
		
		for image in os.listdir(path+"/"+file):
		
			if image.endswith(".jpg"):
				print(path+"/"+file+"/"+image)
				img = cv2.imread(path+"/"+file+"/"+image)
				_, img_encoded = cv2.imencode('.jpg', img)
				response = requests.post(test_url, data=img_encoded.tostring(), headers=headers)
				gender_dictionary[file]= response.text
				print(response.text)

			break
	return gender_dictionary

def insert_to_database(dict):
	conn = psycopg2.connect(database="Bottle", user = "postgres", password = "bottle", host = "127.0.0.1", port = "5432")
	print("Opened database successfully")
	cur = conn.cursor()
	for key, value in dict.items():
		print(key+value)
		cur.execute("INSERT INTO models_pos_person (person_id,ethnicity,gender,age) VALUES ( '"+key+"', 'eeet', '"+value+"', '21' )");

	conn.commit()
	print("Records created successfully")
	conn.close()

dic = search_and_send()
print(dic)
insert_to_database(dic)

# # prepare headers for http request
# content_type = 'image/jpeg'
# headers = {'content-type': content_type}

# img = cv2.imread('pree1298.jpg')
# # encode image as jpeg
# _, img_encoded = cv2.imencode('.jpg', img)
# # send http request with image and receive response
# response = requests.post(test_url, data=img_encoded.tostring(), headers=headers)
# # decode response
# print(response.text)