import keras
from tensorflow.keras.models import load_model
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform
from keras.preprocessing import image
from PIL import Image
import numpy as np


model = load_model('fine-tune_gender_model.h5')

test_image = image.load_img("pree1298.jpg", target_size = (96, 96)) 
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = model.predict(test_image)
print ("Female: " + str(result[0][0] * 100) + "%" if result[0][0]>result[0][1] else "Male: " + str(result[0][1] * 100) + "%")