from flask import Flask, jsonify, request
import random
import time
import keras
from tensorflow.keras.models import load_model
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform
from keras.preprocessing import image
from PIL import Image
import numpy as np
import cv2


app = Flask(__name__)

@app.route("/api/funny")
def serve_funny_quote():
	quotes = funny_quotes()
	print(very)
	no_of_quotes = len(quotes)
	selected_quote = quotes[random.randint(0,no_of_quotes-1)]
	return jsonify(selected_quote)

@app.route("/gender_discriminator", methods=["POST"])
def discriminate():
    img = Image.open(request.files['file'])
    # test_image = image.load_img(img, target_size = (96, 96))
    model = load_model('fine-tune_gender_model.h5') 
    test_image = image.img_to_array(img)
    test_image = np.expand_dims(test_image, axis = 0)
    result = model.predict(test_image)
   # print ("Female: " + str(result[0][0] * 100) + "%" if result[0][0]>result[0][1] else "Male: " + str(result[0][1] * 100) + "%")
    request.close()
    return "Female" if result[0][0]>result[0][1] else "Male"

@app.route('/gender', methods=['POST'])
def gender():
    r = request
    # convert string of image data to uint8
    nparr = np.fromstring(r.data, np.uint8)
    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    
    model = load_model('fine-tune_gender_model.h5') 
    test_image = image.img_to_array(img)
    test_image = np.expand_dims(test_image, axis = 0)
    result = model.predict(test_image)
    #print ("Female: " + str(result[0][0] * 100) + "%" if result[0][0]>result[0][1] else "Male: " + str(result[0][1] * 100) + "%")
    return "Female" if result[0][0]>result[0][1] else "Male"



# start flask app
app.run(host="0.0.0.0", port=5000)

if __name__ == "__main__":
	print("Sleeping start")
	very = "Variable set"
	print("Sleeping end")
	app.run(debug=True)